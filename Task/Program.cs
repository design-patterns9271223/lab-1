public interface IMoney
{
    int Major { get; set; }
    int Minor { get; set; }
    void Print();
    void Assign(int major, int minor);
}

public class Money : IMoney
{
    private int _major;
    private int _minor;

    public int Major
    {
        get { return _major; }
        set
        {
            if (value < 0)
                throw new ArgumentException("Major part cannot be negative");
            _major = value;
        }
    }

    public int Minor
    {
        get { return _minor; }
        set
        {
            if (value < 0)
                throw new ArgumentException("Minor part cannot be negative");
            _minor = value;
        }
    }

    public Money(int major, int minor)
    {
        Major = major;
        Minor = minor;
    }

    public void Print()
    {
        Console.WriteLine($"Amount: {Major}.{Minor:D2}");
    }

    public void Assign(int major, int minor)
    {
        Major = major;
        Minor = minor;
    }
}

public class Product
{
    public IMoney Cost { get; set; }
    public string Description { get; set; }
    public string UnitType { get; set; }
    public int Amount { get; set; }
    public DateTime EntryDate { get; set; }

    public Product(string description, IMoney cost, string unitType, int amount, DateTime entryDate)
    {
        Description = description;
        Cost = cost;
        UnitType = unitType;
        Amount = amount;
        EntryDate = entryDate;
    }

    public void LowerPrice(int decrement)
    {
        Cost.Major -= decrement;
    }
}

public class Warehouse
{
    public string Location { get; set; }
    public List<Product> Inventory { get; set; }

    public Warehouse(string location)
    {
        Location = location;
        Inventory = new List<Product>();
    }

    public void InsertProduct(Product product)
    {
        Inventory.Add(product);
    }
}

public class Reporting
{
    public Warehouse Warehouse { get; set; }

    public Reporting(Warehouse warehouse)
    {
        Warehouse = warehouse;
    }

    public void ShowInventoryReport()
    {
        foreach (var product in Warehouse.Inventory)
        {
            Console.WriteLine($"Item: {product.Description}, Cost: {product.Cost.Major}.{product.Cost.Minor:D2}, Unit: {product.UnitType}, Quantity: {product.Amount}, Date Added: {product.EntryDate}");
        }
    }

    public void NoteProductArrival(Product product)
    {
        Warehouse.InsertProduct(product);
        Console.WriteLine($"Item {product.Description} was added to the warehouse on {product.EntryDate}.");
    }

    public void NoteProductDeparture(Product product)
    {
        Warehouse.Inventory.Remove(product);
        Console.WriteLine($"Item {product.Description} was removed from the warehouse.");
    }
}

class Program
{
    static void Main(string[] args)
    {
        IMoney currency1 = new Money(100, 50);
        IMoney currency2 = new Money(200, 25);
        IMoney currency3 = new Money(150, 75);

        Product item1 = new Product("Apple", currency1, "kg", 10, DateTime.Now);
        Product item2 = new Product("Orange", currency2, "kg", 20, DateTime.Now);
        Product item3 = new Product("Banana", currency3, "kg", 15, DateTime.Now);

        Warehouse warehouse = new Warehouse("Main Warehouse");

        Reporting audit = new Reporting(warehouse);

        audit.NoteProductArrival(item1);
        audit.NoteProductArrival(item2);
        audit.NoteProductArrival(item3);

        Console.WriteLine("\nCurrent Inventory:");
        audit.ShowInventoryReport();

        foreach (var item in warehouse.Inventory)
        {
            item.LowerPrice(10);
        }

        Console.WriteLine("\nInventory after price adjustment:");
        audit.ShowInventoryReport();

        audit.NoteProductDeparture(item1);

        Console.WriteLine("\nInventory after item removal:");
        audit.ShowInventoryReport();

        Console.ReadLine();
    }
}
